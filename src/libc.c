#include <stddef.h>
#include <string.h>

void *memcpy (void *dest, const void *src, size_t len)
{
  char *d = dest;
  const char *s = src;
  while (len--)
	*d++ = *s++;
  return dest;
}

size_t strlen(str)
  const char *str;
{
  register const char *s;

  for (s = str; *s; ++s);
  return(s - str);
}

void *memset (void *dest, register int val, register size_t len)
{
  register unsigned char *ptr = (unsigned char*)dest;
  while (len-- > 0)
	*ptr++ = val;
  return dest;
}

char *strcpy (char *dest, const char *src)
{
  return memcpy (dest, src, strlen (src) + 1);
}

