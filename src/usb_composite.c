#include <libopencm3/stm32/gpio.h>
#include <libopencm3/usb/audio.h>
#include <libopencm3/usb/cdc.h>
#include <libopencm3/usb/midi.h>
#include <libopencm3/usb/usbd.h>
#include <stdlib.h>
#include <string.h>
#include "common.h"

static const char *usb_strings[] = {
  "Black Mesa Technologies",
  "Melody Matrix MIDI keyboard",
  SERIAL_NUMBER,
};

static usbd_device *usbd_dev;
uint8_t usbd_control_buffer[512];

static const struct usb_device_descriptor dev_desc = {
  .bLength = USB_DT_DEVICE_SIZE,
  .bDescriptorType = USB_DT_DEVICE,
  .bcdUSB = 0x0200,
  .bDeviceClass = 0,
  .bDeviceSubClass = 0,
  .bDeviceProtocol = 0,
  .bMaxPacketSize0 = 64,
  .idVendor = 0x9986,
  .idProduct = 0x752d,
  .bcdDevice = 0x0200,
  .iManufacturer = 1,
  .iProduct = 2,
  .iSerialNumber = 0,
  .bNumConfigurations = 1,
};

// ENDPOINTS
static const struct usb_endpoint_descriptor comm_endp[] = {{
  .bLength = USB_DT_ENDPOINT_SIZE,
	.bDescriptorType = USB_DT_ENDPOINT,
	.bEndpointAddress = 0x84,
	.bmAttributes = USB_ENDPOINT_ATTR_INTERRUPT,
	.wMaxPacketSize = 16,
	.bInterval = 255,
}};

static const struct usb_endpoint_descriptor data_endp[] = {{
  .bLength = USB_DT_ENDPOINT_SIZE,
	.bDescriptorType = USB_DT_ENDPOINT,
	.bEndpointAddress = 0x02,
	.bmAttributes = USB_ENDPOINT_ATTR_BULK,
	.wMaxPacketSize = 64,
	.bInterval = 1,
}, {
  .bLength = USB_DT_ENDPOINT_SIZE,
	.bDescriptorType = USB_DT_ENDPOINT,
	.bEndpointAddress = 0x83,
	.bmAttributes = USB_ENDPOINT_ATTR_BULK,
	.wMaxPacketSize = 64,
	.bInterval = 1,
}};

static const struct usb_midi_endpoint_descriptor midi_bulk_endp[] = {{
  .head = {
	.bLength = sizeof(struct usb_midi_endpoint_descriptor),
	.bDescriptorType = USB_AUDIO_DT_CS_ENDPOINT,
	.bDescriptorSubType = USB_MIDI_SUBTYPE_MS_GENERAL,
	.bNumEmbMIDIJack = 1,
  },
	.jack[0] = {
	  .baAssocJackID = 0x01,
	},
}, {
  .head = {
	.bLength = sizeof(struct usb_midi_endpoint_descriptor),
	.bDescriptorType = USB_AUDIO_DT_CS_ENDPOINT,
	.bDescriptorSubType = USB_MIDI_SUBTYPE_MS_GENERAL,
	.bNumEmbMIDIJack = 1,
  },
	.jack[0] = {
	  .baAssocJackID = 0x03,
	},
}};

static const struct usb_endpoint_descriptor bulk_endp[] = {{
  .bLength = USB_DT_ENDPOINT_SIZE,
	.bDescriptorType = USB_DT_ENDPOINT,
	.bEndpointAddress = 0x01,
	.bmAttributes = USB_ENDPOINT_ATTR_BULK,
	.wMaxPacketSize = 0x40,
	.bInterval = 0x00,

	.extra = &midi_bulk_endp[0],
	.extralen = sizeof(midi_bulk_endp[0])
}, {
  .bLength = USB_DT_ENDPOINT_SIZE,
	.bDescriptorType = USB_DT_ENDPOINT,
	.bEndpointAddress = 0x81,
	.bmAttributes = USB_ENDPOINT_ATTR_BULK,
	.wMaxPacketSize = 0x40,
	.bInterval = 0x00,

	.extra = &midi_bulk_endp[1],
	.extralen = sizeof(midi_bulk_endp[1])
}};

// DESCRIPTORS & INTERFACES
static const struct {
  struct usb_cdc_header_descriptor header;
  struct usb_cdc_call_management_descriptor call_mgmt;
  struct usb_cdc_acm_descriptor acm;
  struct usb_cdc_union_descriptor cdc_union;
} __attribute__((packed)) cdcacm_functional_descriptors = {
  .header = {
	.bFunctionLength = sizeof(struct usb_cdc_header_descriptor),
	.bDescriptorType = CS_INTERFACE,
	.bDescriptorSubtype = USB_CDC_TYPE_HEADER,
	.bcdCDC = 0x0110,
  },
  .call_mgmt = {
	.bFunctionLength =
	  sizeof(struct usb_cdc_call_management_descriptor),
	.bDescriptorType = CS_INTERFACE,
	.bDescriptorSubtype = USB_CDC_TYPE_CALL_MANAGEMENT,
	.bmCapabilities = 0,
	.bDataInterface = 1,
  },
  .acm = {
	.bFunctionLength = sizeof(struct usb_cdc_acm_descriptor),
	.bDescriptorType = CS_INTERFACE,
	.bDescriptorSubtype = USB_CDC_TYPE_ACM,
	.bmCapabilities = 0,
  },
  .cdc_union = {
	.bFunctionLength = sizeof(struct usb_cdc_union_descriptor),
	.bDescriptorType = CS_INTERFACE,
	.bDescriptorSubtype = USB_CDC_TYPE_UNION,
	.bControlInterface = 2,
	.bSubordinateInterface0 = 3,
  },
};

static const struct usb_interface_descriptor comm_iface[] = {{
  .bLength = USB_DT_INTERFACE_SIZE,
	.bDescriptorType = USB_DT_INTERFACE,
	.bInterfaceNumber = 2,
	.bAlternateSetting = 0,
	.bNumEndpoints = 1,
	.bInterfaceClass = USB_CLASS_CDC,
	.bInterfaceSubClass = USB_CDC_SUBCLASS_ACM,
	.bInterfaceProtocol = USB_CDC_PROTOCOL_AT,
	.iInterface = 0,
	.endpoint = comm_endp,
	.extra = &cdcacm_functional_descriptors,
	.extralen = sizeof(cdcacm_functional_descriptors),
}};

static const struct usb_interface_descriptor data_iface[] = {{
  .bLength = USB_DT_INTERFACE_SIZE,
	.bDescriptorType = USB_DT_INTERFACE,
	.bInterfaceNumber = 3,
	.bAlternateSetting = 0,
	.bNumEndpoints = 2,
	.bInterfaceClass = USB_CLASS_DATA,
	.bInterfaceSubClass = 2,
	.bInterfaceProtocol = 1,
	.iInterface = 0,
	.endpoint = data_endp,
}};

static const struct {
  struct usb_audio_header_descriptor_head header_head;
  struct usb_audio_header_descriptor_body header_body;
} __attribute__((packed)) audio_control_functional_descriptors = {
  .header_head = {
	.bLength = sizeof(struct usb_audio_header_descriptor_head) +
	  1 * sizeof(struct usb_audio_header_descriptor_body),
	.bDescriptorType = USB_AUDIO_DT_CS_INTERFACE,
	.bDescriptorSubtype = USB_AUDIO_TYPE_HEADER,
	.bcdADC = 0x0100,
	.wTotalLength =
	  sizeof(struct usb_audio_header_descriptor_head) +
	  1 * sizeof(struct usb_audio_header_descriptor_body),
	.binCollection = 1,
  },
  .header_body = {
	.baInterfaceNr = 0x01,
  },
};

static const struct usb_interface_descriptor audio_control_iface[] = {{
  .bLength = USB_DT_INTERFACE_SIZE,
	.bDescriptorType = USB_DT_INTERFACE,
	.bInterfaceNumber = 0,
	.bAlternateSetting = 0,
	.bNumEndpoints = 0,
	.bInterfaceClass = USB_CLASS_AUDIO,
	.bInterfaceSubClass = USB_AUDIO_SUBCLASS_CONTROL,
	.bInterfaceProtocol = 0,
	.iInterface = 0,

	.extra = &audio_control_functional_descriptors,
	.extralen = sizeof(audio_control_functional_descriptors)
} };

static const struct {
  struct usb_midi_header_descriptor header;
  struct usb_midi_in_jack_descriptor in_embedded;
  struct usb_midi_in_jack_descriptor in_external;
  struct usb_midi_out_jack_descriptor out_embedded;
  struct usb_midi_out_jack_descriptor out_external;
} __attribute__((packed)) midi_streaming_functional_descriptors = {
  .header = {
	.bLength = sizeof(struct usb_midi_header_descriptor),
	.bDescriptorType = USB_AUDIO_DT_CS_INTERFACE,
	.bDescriptorSubtype = USB_MIDI_SUBTYPE_MS_HEADER,
	.bcdMSC = 0x0100,
	.wTotalLength = sizeof(midi_streaming_functional_descriptors),
  },
  .in_embedded = {
	.bLength = sizeof(struct usb_midi_in_jack_descriptor),
	.bDescriptorType = USB_AUDIO_DT_CS_INTERFACE,
	.bDescriptorSubtype = USB_MIDI_SUBTYPE_MIDI_IN_JACK,
	.bJackType = USB_MIDI_JACK_TYPE_EMBEDDED,
	.bJackID = 0x01,
	.iJack = 0x00,
  },
  .in_external = {
	.bLength = sizeof(struct usb_midi_in_jack_descriptor),
	.bDescriptorType = USB_AUDIO_DT_CS_INTERFACE,
	.bDescriptorSubtype = USB_MIDI_SUBTYPE_MIDI_IN_JACK,
	.bJackType = USB_MIDI_JACK_TYPE_EXTERNAL,
	.bJackID = 0x02,
	.iJack = 0x00,
  },
  .out_embedded = {
	.head = {
	  .bLength = sizeof(struct usb_midi_out_jack_descriptor),
	  .bDescriptorType = USB_AUDIO_DT_CS_INTERFACE,
	  .bDescriptorSubtype = USB_MIDI_SUBTYPE_MIDI_OUT_JACK,
	  .bJackType = USB_MIDI_JACK_TYPE_EMBEDDED,
	  .bJackID = 0x03,
	  .bNrInputPins = 1,
	},
	.source[0] = {
	  .baSourceID = 0x02,
	  .baSourcePin = 0x01,
	},
	.tail = {
	  .iJack = 0x00,
	}
  },
  .out_external = {
	.head = {
	  .bLength = sizeof(struct usb_midi_out_jack_descriptor),
	  .bDescriptorType = USB_AUDIO_DT_CS_INTERFACE,
	  .bDescriptorSubtype = USB_MIDI_SUBTYPE_MIDI_OUT_JACK,
	  .bJackType = USB_MIDI_JACK_TYPE_EXTERNAL,
	  .bJackID = 0x04,
	  .bNrInputPins = 1,
	},
	.source[0] = {
	  .baSourceID = 0x01,
	  .baSourcePin = 0x01,
	},
	.tail = {
	  .iJack = 0x00,
	},
  },
};

static const struct usb_interface_descriptor midi_streaming_iface[] = {{
  .bLength = USB_DT_INTERFACE_SIZE,
	.bDescriptorType = USB_DT_INTERFACE,
	.bInterfaceNumber = 1,
	.bAlternateSetting = 0,
	.bNumEndpoints = 2,
	.bInterfaceClass = USB_CLASS_AUDIO,
	.bInterfaceSubClass = USB_AUDIO_SUBCLASS_MIDISTREAMING,
	.bInterfaceProtocol = 0,
	.iInterface = 0,

	.endpoint = bulk_endp,

	.extra = &midi_streaming_functional_descriptors,
	.extralen = sizeof(midi_streaming_functional_descriptors)
}};

static const struct usb_interface ifaces[] = {
  {
	.num_altsetting = 1,
	.altsetting = audio_control_iface,
  }, {
	.num_altsetting = 1,
	.altsetting = midi_streaming_iface,
  }, {
	.num_altsetting = 1,
	.altsetting = comm_iface,
  }, {
	.num_altsetting = 1,
	.altsetting = data_iface,
  }
};


//CONFIG
static const struct usb_config_descriptor config = {
  .bLength = USB_DT_CONFIGURATION_SIZE,
  .bDescriptorType = USB_DT_CONFIGURATION,
  .wTotalLength = 0,
  .bNumInterfaces = 4,
  .bConfigurationValue = 1,
  .iConfiguration = 0,
  .bmAttributes = 0x80,
  .bMaxPower = 0x32,
  .interface = ifaces,
};

// CALLBACKS
  static enum usbd_request_return_codes
cdcacm_control_request(
	usbd_device *usbd_dev,
	struct usb_setup_data *req,
	uint8_t **buf,
	uint16_t *len,
	void (**complete)(usbd_device *usbd_dev, struct usb_setup_data *req))
{
  (void)complete;
  (void)buf;
  (void)usbd_dev;

  switch (req->bRequest) {
	case USB_CDC_REQ_SET_CONTROL_LINE_STATE: {
											   /*
												* This Linux cdc_acm driver requires this to be implemented
												* even though it's optional in the CDC spec, and we don't
												* advertise it in the ACM functional descriptor.
												*/
											   char local_buf[10];
											   struct usb_cdc_notification *notif = (void *)local_buf;

											   /* We echo signals back to host as notification. */
											   notif->bmRequestType = 0xA1;
											   notif->bNotification = USB_CDC_NOTIFY_SERIAL_STATE;
											   notif->wValue = 0;
											   notif->wIndex = 0;
											   notif->wLength = 2;
											   local_buf[8] = req->wValue & 3;
											   local_buf[9] = 0;
											   // usbd_ep_write_packet(0x84, buf, 10);
											   return USBD_REQ_HANDLED;
											 }
	case USB_CDC_REQ_SET_LINE_CODING:
											 if (*len < sizeof(struct usb_cdc_line_coding))
											   return USBD_REQ_NOTSUPP;
											 return USBD_REQ_HANDLED;
  }
  return USBD_REQ_NOTSUPP;
}


static void cdcacm_data_rx_cb(usbd_device *usbd_dev, uint8_t ep)
{
  (void)ep;
  char buf_rx[64];
  char buf_tx[4] = { 0x08, 0x01, 0x02, 0x03 };
  const int len_rx = usbd_ep_read_packet(usbd_dev, 0x02, buf_rx, sizeof(buf_rx));
  const int len_tx = sizeof(buf_tx);
  for(int i = 0; i < len_rx; i += len_tx)
  {
	const int len = len_rx - i >= len_tx ? len_tx : len_rx % len_tx;
	memcpy(buf_tx, buf_rx+i, len);
	for(int j=16; usbd_ep_write_packet(usbd_dev, 0x81, buf_tx, len) && j--;);
  }
  gpio_toggle(GPIOC, GPIO13);
  return;
}

static void cdcacm_set_config(usbd_device *usbd_dev, uint16_t wValue)
{
  (void)wValue;
  (void)usbd_dev;

  usbd_ep_setup(usbd_dev, 0x02, USB_ENDPOINT_ATTR_BULK, 64, cdcacm_data_rx_cb);
  usbd_ep_setup(usbd_dev, 0x83, USB_ENDPOINT_ATTR_BULK, 64, NULL);
  usbd_ep_setup(usbd_dev, 0x84, USB_ENDPOINT_ATTR_INTERRUPT, 16, NULL);

  usbd_register_control_callback(
	  usbd_dev,
	  USB_REQ_TYPE_CLASS | USB_REQ_TYPE_INTERFACE,
	  USB_REQ_TYPE_TYPE | USB_REQ_TYPE_RECIPIENT,
	  cdcacm_control_request);
}

/* SysEx identity message, preformatted with correct USB framing information */
const uint8_t sysex_identity[] = {
  0x04,	/* USB Framing (3 byte SysEx) */
  0xf0,	/* SysEx start */
  0x7e,	/* non-realtime */
  0x00,	/* Channel 0 */
  0x04,	/* USB Framing (3 byte SysEx) */
  0x7d,	/* Educational/prototype manufacturer ID */
  0x66,	/* Family code (byte 1) */
  0x66,	/* Family code (byte 2) */
  0x04,	/* USB Framing (3 byte SysEx) */
  0x51,	/* Model number (byte 1) */
  0x19,	/* Model number (byte 2) */
  0x00,	/* Version number (byte 1) */
  0x04,	/* USB Framing (3 byte SysEx) */
  0x00,	/* Version number (byte 2) */
  0x01,	/* Version number (byte 3) */
  0x00,	/* Version number (byte 4) */
  0x05,	/* USB Framing (1 byte SysEx) */
  0xf7,	/* SysEx end */
  0x00,	/* Padding */
  0x00,	/* Padding */
};

static void usbmidi_data_rx_cb(usbd_device *usbd_dev, uint8_t ep)
{
  (void)usbd_dev;
  (void)ep;
  gpio_toggle(GPIOC, GPIO13);
  return;
}

static void usbmidi_set_config(usbd_device *usbd_dev, uint16_t wValue)
{
  (void)wValue;

  usbd_ep_setup(usbd_dev, 0x01, USB_ENDPOINT_ATTR_BULK, 64,
	  usbmidi_data_rx_cb);
  usbd_ep_setup(usbd_dev, 0x81, USB_ENDPOINT_ATTR_BULK, 64, NULL);
}

void usb_composite_init(void)
{
  usbd_dev = usbd_init(&st_usbfs_v1_usb_driver, &dev_desc, &config,
	  usb_strings, 3,
	  usbd_control_buffer, sizeof(usbd_control_buffer));
  usbd_register_set_config_callback(usbd_dev, usbmidi_set_config);
  usbd_register_set_config_callback(usbd_dev, cdcacm_set_config);
}

void usb_composite_poll(){
  usbd_poll(usbd_dev);
}

