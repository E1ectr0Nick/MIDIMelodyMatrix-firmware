#include <libopencm3/cm3/scb.h>
#include <libopencm3/stm32/pwr.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/rtc.h>

#define REG_BOOT 0
#define CMD_BOOT 0x4F42UL //dapboot

#define RTC_BKP_DR(reg)  MMIO16(BACKUP_REGS_BASE + 4 + (4 * (reg)))

void dfu_mode_disable()
{
  pwr_disable_backup_domain_write_protect();
  RTC_BKP_DR(REG_BOOT) = 0;
  pwr_enable_backup_domain_write_protect();

  rcc_periph_clock_disable(RCC_PWR);
  rcc_periph_clock_disable(RCC_BKP);
}


void dfu_mode_enable()
{
  rcc_periph_clock_enable(RCC_PWR);
  rcc_periph_clock_enable(RCC_BKP);

  pwr_disable_backup_domain_write_protect();
  RTC_BKP_DR(REG_BOOT) = CMD_BOOT;
  pwr_enable_backup_domain_write_protect();
}

void dfu_mode_reboot()
{
  dfu_mode_enable();
#if defined(__ARM_ARCH_7M__) || defined(__ARM_ARCH_7EM__)
  scb_reset_core();
#endif
  scb_reset_system();

}
