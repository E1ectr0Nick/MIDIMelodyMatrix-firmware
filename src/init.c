#include <libopencm3/cm3/nvic.h>
#include <libopencm3/stm32/desig.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/iwdg.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/timer.h>

#include "common.h"
#include "dfu_mode.h"
#include "usb_composite.h"

static inline int clock();
static inline int gpio();
static inline int nvic();
static inline int timer();
static inline int usb();
static inline int watchdog();

static inline int clock()
{
  rcc_periph_clock_enable(RCC_GPIOC);
  //reset USB:
  rcc_periph_clock_enable(RCC_GPIOA);
  rcc_periph_clock_enable(RCC_TIM2);
  rcc_clock_setup_pll(&rcc_hse_configs[RCC_CLOCK_HSE8_72MHZ]);
  return 0;
}

static inline int gpio()
{
  gpio_set_mode(GPIOC, GPIO_MODE_OUTPUT_2_MHZ,
	  GPIO_CNF_OUTPUT_PUSHPULL, GPIO13);
  //reset USB:
  gpio_set_mode(GPIOA, GPIO_MODE_INPUT,
	  GPIO_CNF_INPUT_FLOAT, GPIO1);
  return 0;
}

static inline int nvic()
{
  nvic_enable_irq(NVIC_TIM2_IRQ);
  nvic_set_priority(NVIC_TIM2_IRQ, 1);
  return 0;
}

static inline int timer()
{
  /* Set timer start value. */
  TIM_CNT(TIM2) = 1;
  /* Set timer prescaler. 72MHz/1440 => 50000 counts per second. */
  TIM_PSC(TIM2) = 1440;
  /* End timer value. If this is reached an interrupt is generated. */
  //TIM_ARR(TIM2) = 5000;
  TIM_ARR(TIM2) = 8000;
  /* Update interrupt enable. */
  TIM_DIER(TIM2) |= TIM_DIER_UIE;
  /* Start timer. */
  TIM_CR1(TIM2) |= TIM_CR1_CEN;
  return 0;
}

static inline int usb()
{
  //usb_composite_init();
  usb_composite_init();
  return 0;
}

static inline int watchdog()
{
  iwdg_set_period_ms(2000);
  iwdg_start();
  iwdg_reset();
  return 0;
}

int init()
{
  clock();
  gpio();
  nvic();
  timer();
  watchdog();
  usb();
  dfu_mode_enable(); // enter to dfu mode after reset
  desig_get_unique_id_as_string(SERIAL_NUMBER, sizeof(SERIAL_NUMBER));
  return 0;
}

