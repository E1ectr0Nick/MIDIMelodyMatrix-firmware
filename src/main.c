#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/iwdg.h>
#include <libopencm3/stm32/timer.h>

#include "dfu_mode.h"
#include "init.h"
#include "usb_composite.h"

void tim2_isr(void)
{
  static int i=0;
  ++i;
  gpio_toggle(GPIOC, GPIO13); // led blinking
  if( i == 40) // if disabled after boot
	dfu_mode_enable();
  if( i > 12000) // automatic reboot for easy firmware update
	dfu_mode_reboot();
  TIM_SR(TIM2) &= ~TIM_SR_UIF; // clear interrrupt flag
  iwdg_reset();
}

int main(void)
{
  init();
  while(1)
  {
	usb_composite_poll();
  }
  return 0;
}

