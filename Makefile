
TARGET = stm32f103

#FIXME `make TARGET=stm32f103` -> No rule to make target 'stm32f103', needed by 'all'
#TARGET := $(BIN_DIR)/$(TARGET).bin

# Toolchain
PREFIX = arm-none-eabi-
CC = $(PREFIX)gcc
LD = $(PREFIX)ld
OBJDUMP = $(PREFIX)objdump
OBJCOPY =  $(PREFIX)objcopy
MKDIR = mkdir

# Common config
CFLAGS = -Wall -Wextra -pedantic -O2 #-std=c99 -s
CFLAGS += -fno-common -fmessage-length=0 -fno-exceptions
CFLAGS += -ffunction-sections -fdata-sections
CFLAGS += -I./include -I./src/include

LDFLAGS = --gc-sections -X #-nostdlib

# Files
SRC_DIR = src
SRC_FILES = $(shell find $(SRC_DIR) -type f -name '*.c')

OBJ_DIR = obj
OBJ_FILES = $(patsubst $(SRC_DIR)/%.c,$(OBJ_DIR)/%.o,$(SRC_FILES))

BIN_DIR = build

# Target configs
include mk/config.mk
include mk/$(basename $(notdir $(TARGET))).mk

LDFLAGS += -T./linker_scripts/$(basename $(notdir $(TARGET))).ld

# Debug config
ifeq ($(DEBUG), 1)
CFLAGS += -g
#LDFLAGS += --print-gc-sections
.PRECIOUS: $(BIN_DIR)/%.elf
else
LDFLAGS += -s
endif

# Verbose output
ifneq ($(V),1)
Q=@
endif

# Rules
all: $(BIN_DIR)/$(TARGET).bin #TODO rename to $(TARGET)

$(BIN_DIR)/%.srec: $(BIN_DIR)/%.elf
	$Q$(OBJCOPY) -O srec $< $@

$(BIN_DIR)/%.hex: $(BIN_DIR)/%.elf
	$Q$(OBJCOPY) -O ihex $< $@

$(BIN_DIR)/%.bin: $(BIN_DIR)/%.elf
	$Q$(OBJCOPY) -O binary $< $@

$(BIN_DIR)/%.elf: $(OBJ_FILES)
	$Q$(MKDIR) -p $(@D)
	$Q$(LD) $^ $(LDFLAGS) -o $@

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c
	$Q$(MKDIR) -p $(@D)
	$Q$(CC) $(CFLAGS) -c -o $@ $<

clean:
	$Q$(RM) -rf $(OBJ_DIR) $(BIN_DIR)

.PHONY: all clean

