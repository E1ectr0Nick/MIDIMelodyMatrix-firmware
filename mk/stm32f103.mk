CFLAGS += -DSTM32F1
CFLAGS += -mthumb -mcpu=cortex-m3 -msoft-float -mfloat-abi=soft -mfpu=fpv4-sp-d16
CFLAGS += -mlittle-endian -mthumb-interwork

LDFLAGS += -lopencm3_stm32f1

