{
  description = "MIDIMelodyMatrix firmware for ARM uC like STM32F103 Project";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-23.11";
    libopencm3.url = "github:libopencm3/libopencm3";
    libopencm3.flake = false;
  };

  outputs = { self, nixpkgs, flake-utils, libopencm3 }
  :flake-utils.lib.eachDefaultSystem
    (system: let pkgs = nixpkgs.legacyPackages.${system}; in with pkgs; {
      devShells.default = mkShell {
        buildInputs = [
          pkgs.bintools
          pkgs.gcc-arm-embedded
          pkgs.stlink # uart
          pkgs.stm32flash # swd
          pkgs.dfu-programmer
          pkgs.dfu-util
        ];
      };
     }
    );
  nixConfig.bash-prompt = "\[\\e[33mdev\\e[m: \\e[32m$(pwd)\\e[m\]$ ";
}

