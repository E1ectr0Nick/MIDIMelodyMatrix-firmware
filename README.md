# Build
## Linux
If you are using NixOS type `nix develop` for install all dependencies and continue next steps.
```sh
make
st-flash write build/stm32f103-dfu.bin 0x08000000
```

Use mk/* files as target like this:
```sh
make TARGET=stm32f103-dfu
```

You must install DFU bootloader if you want update firmware via USB late.
```sh
git clone https://github.com/devanlai/dapboot
cd src
make TARGET=BLUEPILL
st-flash write dapboot.bin 0x08000000
#firmware donwload of this app
st-flash write midi-keyboard-stm32f103-dfu.bin 0x08002000
```

Firmware via USART:
Set boot pins to `BOOT0=0` & `BOOT1=1` then run:
```sh
stm32flash -w dapboot.bin -b 115200 -v -g 0 /dev/ttyUSB0
```

Update via usb:
```sh
dfu-util -D midi-keyboard-stm32f103-dfu.bin
```

# Update firmware in Windows
In windows you can use WebDFU:
1. open web browser
2. plug device in DFU mode.
3. select device

